FROM debian:latest

LABEL maintainer="jrc <jrc@j12d.me>"

ADD https://download-cdn.resilio.com/stable/linux-x64/resilio-sync_x64.tar.gz /var/local/rslsync/rslsync.tgz

RUN adduser \
    --disabled-password \
    --quiet \
    rslsync 

ADD rslsync.conf /var/local/rslsync/rslsync.conf
ADD init /init 

WORKDIR /var/local/rslsync/

RUN /bin/bash -c ' \
    apt-get update \
      && apt-get -y install gettext-base \
      curl \
      && apt-get -y upgrade \
      && apt-get clean \
      && tar -xzf rslsync.tgz \
      && rm -f rslsync.tgz \
      && chmod +x rslsync \
      && mkdir /var/local/rslsync/.sync \
      && mkdir /var/local/rslsync/data/ \
      && mkdir /opt/system/ \
      '

EXPOSE 8888:8888

ENTRYPOINT ["bash", "/init"]
