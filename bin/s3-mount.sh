#!/bin/bash

CACHE_DIR=/tmp/cache
S3_BUCKET=jrc-rslsync
MNT_DIR=/mnt/rslsync2

mkdir -p ${MNT_DIR}

if [ -f ~/.passwd-s3fs ] ; then
	echo "Mount S3 Bucket ${S3_BUCKET} "
	s3fs -o use_cache=${CACHE_DIR} ${S3_BUCKET} ${MNT_DIR}
else
	echo "No passwd file found"
    echo "create ~/.passwd-s3fs and add aws creds AWSKey:AWSSecretKey"
fi
