IMAGE_URL ?= registry.gitlab.com/jrosco/docker-rslsync:latest
PORTS ?= 8888:8888
NAME ?= rslsync 
SHELL ?= bash
# Make this the same as the "directory_root" in file rslsync.conf 
CONTAINER_DIR ?= /home/rslsync
# Map "CONTAINER_DIR" to this directory
HOST_DIR ?= ~/Rslsync
# Make this the same as the "storage_path" path in file rslsync.conf
PERSISTENT_DIR ?= /opt/system
# Map "PERSISTENT" to this directory
SYSTEM_STORAGE ?= /opt/rslsync/system

.DEFAULT_GOAL := all

gitlab-logon:
	docker login registry.gitlab.com

build:
	docker $@ -t $(IMAGE_URL) .

run:
	docker $@ -it -d -p $(PORTS) -v $(HOST_DIR):$(CONTAINER_DIR) \
		-v $(SYSTEM_STORAGE):$(PERSISTENT_DIR) \
		--name $(NAME) $(IMAGE_URL)

run-test:
	docker run -it -p $(PORTS) --name \
		$(NAME) $(IMAGE_URL)

exec:
	docker $@ -it $(NAME) $(SHELL)

push:
	docker $@ $(IMAGE_URL)

pull:
	docker $@ $(IMAGE_URL) 

list-running:
	docker ps 
	docker top $(NAME)

stop:
	docker $@ $(NAME)

rm:
	docker $@ $(NAME)

uninstall: stop rm 
	rm -rf $(SYSTEM_STORAGE)

clean: stop rm

all: build run  

